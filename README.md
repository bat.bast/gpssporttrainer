# GPS Sport Trainer

A simple application to display GPS speed and difference with previous training sessions.

## Getting Started
GPS Sport Trainer is a simple application to display GPS speed and difference with previous training sessions.
This application is only a POC to learn Flutter and Dart language.
It is used every day on a Xiaomi Poco F1 with LineageOS 18.1.
If there is some mistakes on your phone, please report me your model and your screen size.

### Build application
$ flutter pub run flutter_launcher_icons:main # Build icons
$ flutter build apk --target-platform android-arm --release --build-name=0.5.0+2 --build-number=2 # Build apk

### Screenshots
<img src="images/GPSSportTrainer_Graph.png" alt="GPS Sport Trainer Graph Screenshot" height="300"/>
<img src="images/GPSSportTrainer_Map.png" alt="GPS Sport Trainer Map Screenshot" height="300"/>
<img src="images/GPSSportTrainer_Courses.png" alt="GPS Sport Trainer Courses Screenshot" height="300ver"/>