import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:gps_sport_trainer/config/uiconfig.dart';
import 'package:gps_sport_trainer/gst/app.dart';

void main() {
  Get.lazyPut<ThemeController>(() => ThemeController());
  return runApp(GPSSportTrainerApp());
}
