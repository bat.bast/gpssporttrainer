import 'dart:math';

import 'package:geolocator/geolocator.dart';
import 'package:gps_sport_trainer/utils/calculus.dart';
import 'package:gps_sport_trainer/utils/logger.dart';
import 'package:gps_sport_trainer/utils/utils.dart' as utils;
import 'package:gpx/gpx.dart';
import 'package:uuid/uuid.dart';

class GSTCourse {
  String id = const Uuid().v4();
  String icon = 'ICON';
  Position? _currentPosition;
  Position? _previousPosition;
  Duration? duration;
  Duration _runDuration = const Duration();
  Duration _pauseDuration = const Duration();
  DateTime startTime = DateTime.now();
  late DateTime endTime;

  late String _referenceCourseTitle;
  DateTime? _pauseStartTime;
  final List<Wpt> _recordPoints = [];
  final List<Map<String, int>> _differencePoints = [];
  late List<Wpt> _referenceRecordPoints;
  bool referenceSourceEnabled = false;

  double distance = 0;
  double _currentSpeed = 0;
  double _previousSpeed = 0;
  double speedAverage = 0;

  int _indexReferenceRecord = 0;
  final int _referenceWptSearchStepMax = 50;

  GSTCourse() {
    /*
    this.currentPosition = new Position(
      longitude: 0.0,
      latitude: 0.0,
      timestamp: new DateTime(0),
      accuracy: 0.0,
      altitude: 0.0,
      heading: 0.0,
      speed: 0.0,
      speedAccuracy: 0.0
    );*/
  }

  GSTCourse.buildReferenceCourseForGPX(
      String startTime, String endTime, String referenceGpx) {
    _referenceCourseTitle = utils.buildCourseTitle(startTime, endTime);
    Gpx gpx = GpxReader().fromString(referenceGpx);
    _referenceRecordPoints = gpx.wpts;
  }

  void addReferenceCourse(GSTCourse reference) async {
    _referenceRecordPoints = reference._referenceRecordPoints;
    _referenceCourseTitle = reference._referenceCourseTitle;
    _indexReferenceRecord = 0;
    referenceSourceEnabled = true;
  }

  set currentPosition(Position? p) {
    if (p != null) {
      _currentPosition ??= p;
      _previousPosition = _currentPosition;
      _currentPosition = p;
      _previousSpeed = _currentSpeed;
      _currentSpeed = speedKMPerHourFromPosition(p);
      // Do not update distance if speed is not >= 0
      // ==> Location changing because of GPS adjustment (precision correction)
      if (p.speed > 0) {
        distance +=
            distanceMeterFromPositions(_previousPosition, _currentPosition);
      }

      _runDuration = p.timestamp!.difference(startTime.add(_pauseDuration));
      speedAverage = averageSpeedKMPerHour(_runDuration, distance);
      endTime = DateTime.now();

      Wpt wpt = Wpt(
          lat: p.latitude,
          lon: p.longitude,
          ele: p.altitude,
          time: p.timestamp,
          extensions: {
            'speed': _currentSpeed.toString(),
            'speedAverage': utils.getSpeedStr(speedAverage),
            'duration': _runDuration.inSeconds.toString(),
            'distance': utils.getDistanceMStr(distance),
          });
      GSTLogger.logger.d('New $wpt');
      _recordPoints.add(wpt);

      if (referenceSourceEnabled) {
        List res = _fetchReferenceNearestPointFromDistance(distance);
        Wpt refWpt = res[0];
        double distanceToReference = res[1];
        if (refWpt != null) {
          GSTLogger.logger.d(
              'Reference point $refWpt - Distance difference = $distanceToReference m');

          // Correct the duration with speed average at this Wpt
          double wptSpeedAverage =
              double.parse(refWpt.extensions['speedAverage'] as String); // km/h
          double refWptDuration =
              double.parse(refWpt.extensions['duration'] as String);
          if (wptSpeedAverage > 0) {
            // 12 km --> 1 h
            // diff --> x = diff * 3.6/12 --> à enlever du temps total
            refWptDuration -= distanceToReference * 3.6 / wptSpeedAverage;
          }

          _differencePoints.add({
            'duration': _runDuration.inSeconds,
            'difference': refWptDuration.round() - _runDuration.inSeconds
          });
          GSTLogger.logger.d(
              'Adding difference point ${_differencePoints[_differencePoints.length - 1]}');
        }
      }
    }
  }

  void pauseStartTime() {
    _pauseStartTime = DateTime.now();
  }

  void updatePauseDuration() {
    _pauseDuration += differenceDateTime(DateTime.now(), _pauseStartTime);
  }

  void updateRunDuration() {
    _runDuration =
        differenceDateTime(DateTime.now(), startTime.add(_pauseDuration));
  }

  double get currentSpeed {
    return _currentSpeed;
  }

  double get previousSpeed {
    return _previousSpeed;
  }

  Duration get runDuration {
    return _runDuration;
  }

  List<Wpt> get recordPoints {
    return _recordPoints;
  }

  List<Map<String, int>> get differencePoints {
    return _differencePoints;
  }

  String _recordsToGPXString() {
    Gpx gpx = Gpx();
    //gpx.creator = "GPS Sport Trainer";
    gpx.wpts = _recordPoints;
    return GpxWriter().asString(gpx, pretty: false);
  }

  String get courseTitle {
    return utils.buildCourseTitle(startTime.toString(), endTime.toString());
  }

  String get referenceCourseTitle {
    return _referenceCourseTitle;
  }

  List _fetchReferenceNearestPointFromDistance(double distance) {
    double? distanceToReference;
    int upperIndex = min(_indexReferenceRecord + _referenceWptSearchStepMax,
        _referenceRecordPoints.length - 1);
    GSTLogger.logger.d(
        'Fetch reference point (distance based criteria) - distance=$distance - _indexReferenceRecord=$_indexReferenceRecord - _referenceRecordPoints.length=$_referenceRecordPoints.length - upperIndex=$upperIndex');
    // There is no more reference points
    if (_indexReferenceRecord == _referenceRecordPoints.length - 1) {
      return [null, distanceToReference];
    }

    Iterable<Wpt> rangeWpt =
        _referenceRecordPoints.getRange(_indexReferenceRecord, upperIndex);
    // We want to find the first point with Wpt distance > current distance
    for (Wpt wpt in rangeWpt) {
      distanceToReference = distance -
          double.parse(wpt.extensions['distance']
              as String); // Must be < 0, otherwise fetch the next point
      GSTLogger.logger.d('Calculus - distanceToReference=$distanceToReference');
      if (distanceToReference > 0) {
        _indexReferenceRecord++;
      } else {
        return [wpt, distanceToReference];
      }
    }
    return [null, distanceToReference];
  }

  int get countRecordPoints {
    return recordPoints.length;
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'startTime': utils.getDateTimeStr(startTime),
      'endTime': utils.getDateTimeStr(endTime),
      'distance': utils.getDistanceMStr(distance),
      'duration': runDuration.inSeconds,
      'speedAverage': utils.getSpeedStr(speedAverage),
      'icon': icon,
      'gpx': _recordsToGPXString()
    };
  }

  String toString() {
    return "${utils.getDateTimeStr(startTime)} ➼ ${utils.getDateTimeStr(endTime)} - ${utils.getDistanceMStr(distance)} m - ${_runDuration.inSeconds.toString()} s - ${utils.getSpeedStr(speedAverage)} km/h";
  }
}
