import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_swiper_tv/flutter_swiper.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:gps_sport_trainer/config/config.dart';
import 'package:gps_sport_trainer/config/uiconfig.dart';
import 'package:gps_sport_trainer/course/course.dart';
import 'package:gps_sport_trainer/database/gstdatabase.dart';
import 'package:gps_sport_trainer/gst/coursespage.dart';
import 'package:gps_sport_trainer/utils/logger.dart';
import 'package:gps_sport_trainer/utils/utils.dart' as utils;
import 'package:gps_sport_trainer/widget/gstmapwidget.dart';
import 'package:gpx/gpx.dart';
import 'package:latlong2/latlong.dart';
import 'package:syncfusion_flutter_charts/charts.dart' as charts;
import 'package:syncfusion_flutter_gauges/gauges.dart' as gauges;
import 'package:wakelock/wakelock.dart';

// Main GPSSportTrainer Page
class GSTSpeedPage extends StatefulWidget {
  GSTSpeedPage({Key? key}) : super(key: key);

  @override
  _GSTSpeedPageState createState() => _GSTSpeedPageState();
}

class _Metrics {
  double speed;
  double speedAverage;
  double distance;
  bool gpsEnabled;

  _Metrics(this.speed, this.speedAverage, this.distance, this.gpsEnabled);

  _Metrics getMetricsUpdated(
      {double? speed,
      double? speedAverage,
      double? distance,
      bool? gpsEnabled}) {
    return _Metrics(
      speed ?? this.speed,
      speedAverage ?? this.speedAverage,
      distance ?? this.distance,
      gpsEnabled ?? this.gpsEnabled,
    );
  }
}

class _GSTSpeedPageState extends State<GSTSpeedPage> {
  final GSTConfig config = GSTConfig();
  final GSTUIConfig uiconfig = GSTUIConfig();
  final GSTDatabase _database = GSTDatabase();

  late GSTCourse _course;

  late charts.ChartSeriesController? _speedChartSeriesController;
  late charts.ChartSeriesController? _averageSpeedChartSeriesController;
  late charts.ChartSeriesController? _differenceChartSeriesController;

  late Widget _graphsWidget;

  final SwiperControl _swiperControl = const SwiperControl(
    iconPrevious: Icons.arrow_back_ios,
    iconNext: Icons.arrow_forward_ios,
    color: Colors.purpleAccent,
    size: 40.0,
    padding: EdgeInsets.all(1.0),
  );
  final SwiperPagination _swiperPagination = const SwiperPagination(
      alignment: Alignment.bottomRight,
      margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 5.0),
      builder: DotSwiperPaginationBuilder(
        color: Colors.black38,
        activeColor: Colors.purpleAccent,
        size: 6.0,
        activeSize: 10.0,
      ));

  late Timer _timerSmooth, _timerDifferenceDatetime;

  late bool _applicationInPause;
  String _textCourseLoaded = "Load Course";

  int _indexGraphRecordsViewed = 0;

  late ValueNotifier _metricsNotifier;
  late ValueNotifier _durationNotifier;
  late ValueNotifier _nowStrNotifier;
  late ValueNotifier _pauseApplicationNotifier;
  late ValueNotifier _saveCourseNotifier;

  bool _viewingGraphWidget = false;
  bool _rebuildGraphsWidget = false;
  bool _backFromMapWidget =
      false; // Using this variable into _updateGraphsMetrics else double line for each data

  late StreamSubscription<Position> _positionStream;

  double _smoothSpeedUpdate = 0;
  int _smoothCounter = 1;

  late ThemeMode _themeMode = ThemeController.to.themeMode;

  _GSTSpeedPageState() {
    GSTLogger.logger.d('Create _GSTSpeedPageState');
    _timerDifferenceDatetime = Timer.periodic(
        Duration(milliseconds: config.durationUpdateInterval),
        (_timerDD) => _updateRunDuration());
    _initApplication();
    _initPermissions();
    _getGPSPosition();
  }

  void _initPermissions() async {
    LocationPermission permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied ||
        permission == LocationPermission.deniedForever) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied ||
          permission == LocationPermission.deniedForever) {
        //await Geolocator.openAppSettings();
        await Geolocator.openLocationSettings();
        GSTLogger.logger.e('Location permissions are denied');
      }
    }
  }

  void _resetApplication() {
    _initApplication();
    _rebuildWidgets();
  }

  void _rebuildWidgets() {
    GSTLogger.logger.d('Rebuild Widgets');
    if (mounted) {
      setState(() {});
    }
  }

  void _initApplication() {
    GSTLogger.logger.d('Initialize Application');
    Wakelock.enable();
    _course = GSTCourse();
    _applicationInPause = false;
    _rebuildGraphsWidget = true;
    _textCourseLoaded = "Load Course";
    _indexGraphRecordsViewed = 0;

    _speedChartSeriesController = null;
    _averageSpeedChartSeriesController = null;
    _differenceChartSeriesController = null;

    _metricsNotifier = ValueNotifier(_Metrics(
      0.0, // speed
      0.0, // speedAverage
      0.0, // distance
      false, // GPS Enabled
    ));
    _durationNotifier = ValueNotifier(const Duration());
    _nowStrNotifier = ValueNotifier(utils.getNowStr());
    _pauseApplicationNotifier =
        ValueNotifier(const Icon(Icons.run_circle, color: Colors.white));
    _saveCourseNotifier = ValueNotifier("Save Course");
  }

  Future<void> _getGPSPosition() async {
    bool gpsEnabled = await Geolocator.isLocationServiceEnabled();
    GSTLogger.logger.d('GPS Status = ' + gpsEnabled.toString());
    if (gpsEnabled) {
      LocationSettings locationSettings = AndroidSettings(
          accuracy: LocationAccuracy.best,
          distanceFilter: 0,
          forceLocationManager: true,
          intervalDuration: config.sensorDurationUpdateInterval,
          foregroundNotificationConfig: const ForegroundNotificationConfig(
            notificationText: "GPSSportTrainer is running in Background",
            notificationTitle: "GST is running in Background",
            enableWakeLock: true,
          ));
      _positionStream =
          Geolocator.getPositionStream(locationSettings: locationSettings)
              .listen((Position? position) {
        if (!_applicationInPause && position != null) {
          _course.currentPosition = position;
          _updateLocalMetrics();
          _updateGraphsMetrics();
        }
      });
    }
  }

  void _pauseApplication() {
    GSTLogger.logger.d('Pausing application : ' +
        _applicationInPause.toString() +
        ' -> ' +
        (!_applicationInPause).toString());
    _applicationInPause = !_applicationInPause;
    if (_applicationInPause) {
      _pauseApplicationNotifier.value =
          const Icon(Icons.pause, color: Colors.red);
      _course.pauseStartTime();
    } else {
      _pauseApplicationNotifier.value =
          const Icon(Icons.run_circle, color: Colors.white);
      _course.updatePauseDuration();
    }
  }

  Future _loadCoursesPage(context) async {
    Navigator.push(
      context,
      // Pass Course object in order to ad to this reference Course from CoursePage
      MaterialPageRoute(
          builder: (context) => CoursesManagePage(
                database: _database,
                course: _course,
              )),
    );
  }

  Future<void> _saveCourse() async {
    _database.insertCourse(_course);
    _saveCourseNotifier.value = _course.courseTitle;
  }

  void _toggleTheme() {
    var oldThemeMode = _themeMode;
    if (_themeMode == ThemeMode.light) {
      _themeMode = ThemeMode.dark;
    } else {
      _themeMode = ThemeMode.light;
    }
    GSTLogger.logger.d('Toggle Theme from $oldThemeMode --> $_themeMode');
    setState(() {
      ThemeController.to.setThemeMode(_themeMode);
    });
  }

  void _updateRunDuration() {
    if (!_applicationInPause) {
      _course.updateRunDuration();
      GSTLogger.logger.v('Updating _durationNotifier');
      _durationNotifier.value = _course.runDuration;
    }
    String currentNowStr = utils.getNowStr();
    if (_nowStrNotifier.value != currentNowStr) {
      GSTLogger.logger.v('Updating _nowStrNotifier');
      _nowStrNotifier.value = currentNowStr;
    }
  }

  void _smoothUpdateSpeed() {
    _incrementSpeed(_smoothSpeedUpdate);
    if (_smoothCounter < config.smoothMaxSteps) {
      _smoothCounter += 1;
      _timerSmooth = Timer(Duration(milliseconds: config.smoothUpdateInterval),
          _smoothUpdateSpeed);
    }
  }

  void _updateLocalMetrics() {
    double speedDiff = _course.currentSpeed - _course.previousSpeed;
    if (speedDiff.abs() > config.minSmoothMetricsUpdate) {
      _smoothSpeedUpdate = speedDiff / config.smoothMaxSteps;
      _smoothCounter = 1;
      _smoothUpdateSpeed();
    } else {
      _incrementSpeed(speedDiff);
    }
  }

  void _incrementSpeed(double speedIncrement) {
    GSTLogger.logger.v('Updating _metricsNotifier');
    _Metrics m = _metricsNotifier.value;
    // If increment speed, then GPS is enabled!
    _metricsNotifier.value = m.getMetricsUpdated(
        speed: _course.previousSpeed + speedIncrement,
        speedAverage: _course.speedAverage,
        distance: _course.distance,
        gpsEnabled: true);
  }

  void _updateGraphsMetrics() {
    if (mounted) {
      if (_viewingGraphWidget && _course.recordPoints.isNotEmpty) {
        if (_backFromMapWidget) {
          _backFromMapWidget = false;
        } else {
          GSTLogger.logger.v('Updating GraphsMetrics');
          if (_speedChartSeriesController != null &&
              _averageSpeedChartSeriesController != null) {
            _speedChartSeriesController!
                .updateDataSource(addedDataIndex: _indexGraphRecordsViewed);
            _averageSpeedChartSeriesController!
                .updateDataSource(addedDataIndex: _indexGraphRecordsViewed);
            // A reference course has been added, but specific graph not added to Widget
            if (_course.referenceSourceEnabled &&
                _differenceChartSeriesController == null) {
              _rebuildGraphsWidget = true;
              _rebuildWidgets();
            }
            if (_course.referenceSourceEnabled &&
                _differenceChartSeriesController != null) {
              _differenceChartSeriesController!
                  .updateDataSource(addedDataIndex: _indexGraphRecordsViewed);
            }
            // Put index to current course length
            _indexGraphRecordsViewed = _course.countRecordPoints - 1;
          }
        }
      }
    }
  }

  List<charts.ChartSeries> _buildGraphsDataSeries() {
    GSTLogger.logger.d('Building GraphsDataSeries');
    // First series is speed
    var speedSeries = charts.SplineSeries<Wpt, DateTime>(
      onRendererCreated: (charts.ChartSeriesController controller) {
        _speedChartSeriesController = controller;
      },
      name: "Speed",
      dataSource: _course.recordPoints,
      xValueMapper: (Wpt point, _) => DateTime(
          0, 0, 0, 0, 0, int.parse(point.extensions['duration'] as String)),
      yValueMapper: (Wpt point, _) =>
          double.parse(point.extensions['speed'] as String),
      width: 5,
      color: uiconfig.speedColor,
    );
    // Second series is speed average
    var speedAverageSeries = charts.SplineSeries<Wpt, DateTime>(
      onRendererCreated: (charts.ChartSeriesController controller) {
        _averageSpeedChartSeriesController = controller;
      },
      name: "Average Speed",
      yAxisName: 'leftY',
      dataSource: _course.recordPoints,
      xValueMapper: (Wpt point, _) => DateTime(
          0, 0, 0, 0, 0, int.parse(point.extensions['duration'] as String)),
      yValueMapper: (Wpt point, _) =>
          double.parse(point.extensions['speedAverage'] as String),
      width: 5,
      color: uiconfig.speedAverageColor,
    );
    if (_course.referenceSourceEnabled) {
      // Third series is difference with previous Course
      var speedDifferenceSeries = charts.SplineSeries<Map, DateTime>(
        onRendererCreated: (charts.ChartSeriesController controller) {
          _differenceChartSeriesController = controller;
        },
        name: "Difference",
        yAxisName: 'rightY',
        dataSource: _course.differencePoints,
        xValueMapper: (Map point, _) =>
            DateTime(0, 0, 0, 0, 0, point['duration']),
        yValueMapper: (Map point, _) => point['difference'],
        width: 5,
        color: uiconfig.differenceWithPrevious,
        dashArray: const <double>[5, 5],
      );
      return <charts.SplineSeries>[
        speedSeries,
        speedAverageSeries,
        speedDifferenceSeries,
      ];
    } else {
      return <charts.SplineSeries>[
        speedSeries,
        speedAverageSeries,
      ];
    }
  }

  @override
  void dispose() {
    _metricsNotifier.dispose();
    _nowStrNotifier.dispose();
    _durationNotifier.dispose();
    _pauseApplicationNotifier.dispose();
    _saveCourseNotifier.dispose();
    _positionStream.cancel();
    _timerSmooth.cancel();
    _timerDifferenceDatetime.cancel();
    super.dispose();
  }

  Widget _buildMapWidget() {
    GSTLogger.logger.d('Building MapWidget');
    List<LatLng> positions = [];
    _course.recordPoints
        .forEach((point) => positions.add(LatLng(point.lat!, point.lon!)));
    return GSTMapWidget(positions: positions);
  }

  Widget _buildGraphsWidget() {
    GSTLogger.logger.d('Building GraphsWidget');
    List<charts.ChartAxis>? axes;
    if (_course.referenceSourceEnabled) {
      axes = <charts.ChartAxis>[
        // Difference
        charts.NumericAxis(
          name: 'rightY',
          title: charts.AxisTitle(
              alignment: charts.ChartAlignment.far,
              text: 'Positive=better, seconds',
              textStyle: TextStyle(
                color: uiconfig.differenceWithPrevious,
                fontSize: 10,
              )),
          axisLine: charts.AxisLine(
              color: uiconfig.differenceWithPrevious,
              width: 1,
              dashArray: <double>[5, 5]),
          opposedPosition: true,
          labelStyle: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 25,
              color: uiconfig.differenceWithPrevious),
          majorGridLines: charts.MajorGridLines(
            width: 1,
            color: uiconfig.differenceWithPrevious,
            dashArray: const <double>[5, 5],
          ),
          majorTickLines:
              charts.MajorTickLines(color: uiconfig.differenceWithPrevious),
        )
      ];
    }

    return charts.SfCartesianChart(
      margin: const EdgeInsets.all(1),
      plotAreaBorderWidth: 0,
      // Abscisse (duration)
      primaryXAxis: charts.DateTimeAxis(
        labelStyle: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 18,
            color: Get.theme.textSelectionTheme.cursorColor),
        majorGridLines: const charts.MajorGridLines(width: 0),
        axisLine: charts.AxisLine(
            color: Get.theme.textSelectionTheme.cursorColor, width: 1),
        majorTickLines: charts.MajorTickLines(
            color: Get.theme.textSelectionTheme.cursorColor),
        // intervalType: charts.DateTimeIntervalType.seconds,
      ),
      // Speed (km/h)
      primaryYAxis: charts.NumericAxis(
        name: 'leftY',
        title: charts.AxisTitle(
            alignment: charts.ChartAlignment.far,
            text: 'km/h',
            textStyle: TextStyle(
              color: Get.theme.textSelectionTheme.cursorColor,
              fontSize: 10,
            )),
        opposedPosition: false,
        labelStyle: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 25,
            color: Get.theme.textSelectionTheme.cursorColor),
        majorGridLines: charts.MajorGridLines(
          width: 1,
          color: Get.theme.textSelectionTheme.cursorColor,
        ),
        axisLine: charts.AxisLine(
            color: Get.theme.textSelectionTheme.cursorColor, width: 1),
        majorTickLines: charts.MajorTickLines(
            color: Get.theme.textSelectionTheme.cursorColor),
      ),
      axes: axes,
      series: _buildGraphsDataSeries(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Get.theme.scaffoldBackgroundColor,
        floatingActionButton: Stack(
          children: <Widget>[
            Padding(
                padding: const EdgeInsets.only(left: 31),
                child: Align(
                    alignment: Alignment.bottomLeft,
                    child: _buildResetApplicationWidget())),
            Align(
                alignment: Alignment.bottomRight,
                child: _buildPauseApplicationWidget()),
            Padding(
                padding: const EdgeInsets.only(left: 31),
                child: Align(
                    alignment: Alignment.bottomCenter,
                    child: _buildLoadCoursesPageWidget())),
            Padding(
                padding: const EdgeInsets.only(top: 51, left: 31),
                child: Align(
                    alignment: Alignment.topLeft,
                    child: _buildSaveCourseWidget())),
            Padding(
                padding: const EdgeInsets.only(top: 51),
                child: Align(
                    alignment: Alignment.topRight,
                    child: _buildToggleThemeWidget()))
          ],
        ),
        body: OrientationBuilder(builder: (context, orientation) {
          return GridView.count(
              // Create a grid with 1 column in portrait mode, 2 columns in landscape mode
              crossAxisCount: orientation == Orientation.portrait ? 1 : 2,
              children: [
                Stack(
                  children: <Widget>[
                    Positioned(
                      top: orientation == Orientation.portrait ? 40 : 50,
                      right: orientation == Orientation.portrait ? 20 : 0,
                      child: _buildGaugesWidget(),
                    ),
                    Positioned(
                      top: orientation == Orientation.portrait ? 175 : 180,
                      right: orientation == Orientation.portrait ? 120 : 100,
                      child: _buildDurationWidget(),
                    ),
                    Positioned(
                      top: orientation == Orientation.portrait ? 220 : 225,
                      right: orientation == Orientation.portrait ? 120 : 100,
                      child: _buildNowStrWidget(),
                    ),
                  ],
                ),
                Padding(
                    padding: EdgeInsets.only(
                        bottom: orientation == Orientation.portrait ? 120 : 50),
                    child: Center(
                      child: _buildSwiperGraphMapWidget(),
                    ))
              ]);
        }));
  }

  Widget _buildPauseApplicationWidget() {
    GSTLogger.logger.d('Building PauseApplicationWidget');
    return ValueListenableBuilder(
        valueListenable: _pauseApplicationNotifier,
        builder: (_, icon, __) => FloatingActionButton(
            // Pause/Unpause button
            heroTag: null,
            onPressed: () => _pauseApplication(),
            tooltip: 'Pause Application',
            child: _pauseApplicationNotifier.value));
  }

  Widget _buildSwiperGraphMapWidget() {
    GSTLogger.logger.d('Building SwiperGraphMapWidget');
    if (_rebuildGraphsWidget) {
      _rebuildGraphsWidget = false;
      _graphsWidget = _buildGraphsWidget();
    }
    return Swiper(
      index: 0,
      itemBuilder: (BuildContext context, int index) {
        GSTLogger.logger.d('Building SwiperWidget for index $index');
        if (index == 0) {
          _viewingGraphWidget = true;
          _backFromMapWidget = true;
          return _graphsWidget;
        } else {
          _viewingGraphWidget = false;
          return _buildMapWidget();
        }
      },
      itemCount: 2,
      pagination: _swiperPagination,
      control: _swiperControl,
      viewportFraction: 1,
      scale: 1,
    );
  }

  Widget _buildToggleThemeWidget() {
    GSTLogger.logger.d('Building ToggleThemeWidget');
    return FloatingActionButton(
        // Toggle theme
        heroTag: null,
        onPressed: () => _toggleTheme(),
        tooltip: 'Toggle Theme',
        child: const Icon(Icons.color_lens, color: Colors.white));
  }

  Widget _buildResetApplicationWidget() {
    GSTLogger.logger.d('Building ResetApplicationWidget');
    return FloatingActionButton(
        // Reset button
        heroTag: null,
        onPressed: () => _resetApplication(),
        tooltip: 'Reset Application',
        child: const Icon(Icons.clear, color: Colors.white));
  }

  Widget _buildLoadCoursesPageWidget() {
    GSTLogger.logger.d('Building LoadCoursesPageWidget');
    return FloatingActionButton.extended(
        // Load Courses page
        heroTag: null,
        onPressed: () => _loadCoursesPage(context),
        tooltip: 'Load Courses Page',
        label: Text(
            _course.referenceSourceEnabled
                ? _course.referenceCourseTitle
                : _textCourseLoaded,
            style: const TextStyle(fontSize: 10)),
        icon: const Icon(Icons.upload_rounded, color: Colors.white));
  }

  Widget _buildSaveCourseWidget() {
    GSTLogger.logger.d('Building SaveCourseWidget');
    return ValueListenableBuilder(
        valueListenable: _saveCourseNotifier,
        builder: (_, title, __) => FloatingActionButton.extended(
            // Save Course
            heroTag: null,
            onPressed: () => _saveCourse(),
            tooltip: 'Save Course',
            label: Text(_saveCourseNotifier.value),
            icon: const Icon(Icons.save, color: Colors.amber)));
  }

  Widget _buildDurationWidget() {
    GSTLogger.logger.d('Building DurationWidget');
    return ValueListenableBuilder(
        valueListenable: _durationNotifier,
        builder: (_, duration, __) => Text(
              utils.getDurationStr(_durationNotifier.value),
              style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 55,
                  color: Colors.amber),
            ));
  }

  Widget _buildNowStrWidget() {
    GSTLogger.logger.d('Building NowStrWidget');
    return ValueListenableBuilder(
        valueListenable: _nowStrNotifier,
        builder: (_, nowStr, __) => Text(
              _nowStrNotifier.value,
              style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 35,
                  color: Colors.amber),
            ));
  }

  Widget _buildGaugesWidget() {
    GSTLogger.logger.d('Building GaugesWidget');
    return ValueListenableBuilder(
        valueListenable: _metricsNotifier,
        builder: (_, metrics, __) => gauges.SfRadialGauge(
                enableLoadingAnimation: true,
                animationDuration: config.bootAnimationDuration,
                axes: <gauges.RadialAxis>[
                  gauges.RadialAxis(
                      startAngle: 180,
                      endAngle: 90,
                      minimum: uiconfig.minSpeed,
                      maximum: uiconfig.maxSpeed,
                      interval: uiconfig.speedInterval,
                      minorTicksPerInterval: uiconfig.minorTicksPerInterval,
                      showFirstLabel: false,
                      labelsPosition: gauges.ElementsPosition.inside,
                      ticksPosition: gauges.ElementsPosition.inside,
                      tickOffset: 2,
                      labelOffset: 20,
                      axisLabelStyle: const gauges.GaugeTextStyle(
                          color: Colors.blueGrey,
                          fontSize: 25,
                          fontStyle: FontStyle.italic,
                          fontWeight: FontWeight.bold),
                      axisLineStyle: const gauges.AxisLineStyle(
                          thickness: 0.15,
                          cornerStyle: gauges.CornerStyle.bothCurve,
                          thicknessUnit: gauges.GaugeSizeUnit.factor),
                      majorTickStyle: const gauges.MajorTickStyle(
                          length: 0.1,
                          lengthUnit: gauges.GaugeSizeUnit.factor,
                          thickness: 1.5,
                          color: Colors.blueGrey),
                      minorTickStyle: const gauges.MinorTickStyle(
                          length: 0.05,
                          lengthUnit: gauges.GaugeSizeUnit.factor,
                          thickness: 1.5,
                          color: Colors.blueGrey),
                      pointers: <gauges.GaugePointer>[
                        gauges.RangePointer(
                            // Current speed
                            value: _metricsNotifier.value.speed,
                            width: 0.15,
                            enableAnimation: true,
                            animationDuration:
                                config.smoothUpdateDelay.toDouble(),
                            cornerStyle: gauges.CornerStyle.bothCurve,
                            sizeUnit: gauges.GaugeSizeUnit.factor,
                            gradient: uiconfig.speedSweepGradient),
                        gauges.MarkerPointer(
                            // Average
                            value: _metricsNotifier.value.speedAverage,
                            markerType: gauges.MarkerType.invertedTriangle,
                            color: uiconfig.speedAverageColor,
                            markerHeight: 27,
                            markerWidth: 15,
                            enableAnimation: true,
                            animationDuration:
                                config.smoothUpdateDelay.toDouble())
                      ],
                      annotations: <gauges.GaugeAnnotation>[
                        gauges.GaugeAnnotation(
                            // Speed Average
                            angle: 125,
                            positionFactor: 1.0,
                            horizontalAlignment: gauges.GaugeAlignment.center,
                            verticalAlignment: gauges.GaugeAlignment.center,
                            widget: Text(
                              utils.getSpeedStr(
                                  _metricsNotifier.value.speedAverage),
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 90,
                                  color: uiconfig.speedAverageColor),
                            )),
                        gauges.GaugeAnnotation(
                            // Distance
                            angle: 280,
                            positionFactor: 0.42,
                            horizontalAlignment: gauges.GaugeAlignment.center,
                            verticalAlignment: gauges.GaugeAlignment.center,
                            widget: Text(
                              utils.getDistanceKiloMetersStr(
                                  _metricsNotifier.value.distance),
                              style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 55,
                                  color: Colors.blueAccent),
                            )),
                        gauges.GaugeAnnotation(
                            // GPS Status
                            axisValue: 12,
                            positionFactor: 0.58,
                            horizontalAlignment: gauges.GaugeAlignment.center,
                            verticalAlignment: gauges.GaugeAlignment.center,
                            widget: Text(
                              'GPS ' +
                                  (_metricsNotifier.value.gpsEnabled
                                      ? 'ON'
                                      : 'OFF'),
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15,
                                  color:
                                      Theme.of(context).colorScheme.secondary),
                            )),
                        gauges.GaugeAnnotation(
                            // Current speed
                            angle: 85,
                            positionFactor: 0.40,
                            horizontalAlignment: gauges.GaugeAlignment.far,
                            widget: Text(
                              utils.getSpeedStr(_metricsNotifier.value.speed),
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 130,
                                  color: uiconfig.speedColor),
                            )),
                        gauges.GaugeAnnotation(
                            // Speed unity
                            angle: 55,
                            positionFactor: 0.62,
                            horizontalAlignment: gauges.GaugeAlignment.far,
                            widget: Text(
                              'km/h',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 25,
                                  color: uiconfig.speedColor),
                            ))
                      ])
                ]));
  }
}
