import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:gps_sport_trainer/config/uiconfig.dart';
import 'package:gps_sport_trainer/gst/speedpage.dart';
import 'package:gps_sport_trainer/utils/logger.dart';

// Main GPS Sport Trainer App
class GPSSportTrainerApp extends StatelessWidget {
  const GPSSportTrainerApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    GSTLogger.logger.d('Create GPSSportTrainerApp');
    ThemeController.to.getThemeModeFromPreferences();
    return GetMaterialApp(
      title: 'GPS Sport Trainer',
      theme: gstLightTheme,
      darkTheme: gstDarkTheme,
      themeMode: ThemeMode.light,
      home: GSTSpeedPage(),
    );
  }
}
