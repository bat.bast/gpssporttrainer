import 'package:flutter/material.dart';
import 'package:gps_sport_trainer/course/course.dart';
import 'package:gps_sport_trainer/database/gstdatabase.dart';
import 'package:gps_sport_trainer/utils/logger.dart';
import 'package:gps_sport_trainer/utils/utils.dart' as utils;

// Manage Courses
class CoursesManagePage extends StatefulWidget {
  final GSTCourse course;
  final GSTDatabase database;

  CoursesManagePage({required this.database, required this.course});

  @override
  _CoursesManagePageState createState() => _CoursesManagePageState();
}

class _CoursesManagePageState extends State<CoursesManagePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Manage Courses'),
        ),
        body: FutureBuilder(
            future: widget.database.getCourses(), //_getCourse(),
            builder: (context, AsyncSnapshot<List> snapshot) {
              switch (snapshot.connectionState) {
                case ConnectionState.none:
                case ConnectionState.waiting:
                  return const Center(
                      child: SizedBox(
                          height: 100,
                          width: 100,
                          child: CircularProgressIndicator()));
                default:
                  if (snapshot.hasError)
                    return Center(child: Text('Error: ${snapshot.error}'));
                  return snapshot.hasData
                      ? ListView.builder(
                          padding: const EdgeInsets.all(8),
                          itemCount: snapshot.data!.length,
                          itemBuilder: (context, index) {
                            return _buildRowWidget(snapshot.data!, index);
                          })
                      : const Center(child: Text('No Course recorded'));
              }
            }));
  }

  Widget _buildRowWidget(List items, int index) {
    GSTLogger.logger.d('Building RowWidget for index $index');
    Map item = items[index];
    String title = utils.buildCourseTitle(item['startTime'], item['endTime']);
    Text titleText = Text(title);
    String subtitle =
        "${item['speedAverage']} km/h | ${utils.getDistanceKiloMetersStr(item['distance'])} km | ${utils.getDurationStr(Duration(seconds: item['duration'].round()))}";
    Text subtitleText = Text(subtitle);
    return Dismissible(
        // Swipe left to right
        background: Container(
          color: Colors.red,
        ),
        // Swipe right to left
        secondaryBackground: Container(
          color: Colors.lightBlue[700],
        ),
        key: Key(item['id']),
        onDismissed: (direction) async {
          if (direction == DismissDirection.startToEnd) {
            widget.database.deleteCourse(item['id']);
            ScaffoldMessenger.of(context)
                .showSnackBar(SnackBar(content: Text("Course $title deleted")));
          } else {
            widget.course.addReferenceCourse(
                await widget.database.loadCourse(item['id']));
            Navigator.pop(context);
          }
        },
        child: Card(
            child: ListTile(
                leading: const Icon(
                  Icons.attach_file,
                  color: Colors.blue,
                ),
                title: titleText,
                subtitle: subtitleText,
                trailing: const Icon(
                  Icons.delete_forever,
                  color: Colors.red,
                ))));
  }
}
