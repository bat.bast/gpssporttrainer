import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class GSTUIConfig {
  const GSTUIConfig();
  static const double _speedMin = 0;
  static const double _speedMax = 35;
  static const double _speedInterval = 5;
  static const double _speedMinorTricksPerInterval = 4;

  static const Color _speedStartColor = Color(0xFF02af00);
  static const Color _speedEndColor = Color(0xFF03ea00);
  static const SweepGradient _speedSweepGradient = SweepGradient(
      colors: <Color>[_speedStartColor, _speedEndColor],
      stops: <double>[0.55, 0.85]);
  static const Color _speedAverageColor = Colors.purpleAccent;
  static const Color _differenceWithPrevious = Colors.amber;

  double get minSpeed => _speedMin;

  double get maxSpeed => _speedMax;

  double get speedInterval => _speedInterval;

  double get minorTicksPerInterval => _speedMinorTricksPerInterval;

  SweepGradient get speedSweepGradient => _speedSweepGradient;

  Color get speedAverageColor => _speedAverageColor;

  Color get speedColor => _speedStartColor;

  Color get differenceWithPrevious => _differenceWithPrevious;
}

final ThemeData gstLightTheme =
    ThemeData.light().copyWith(scaffoldBackgroundColor: Colors.white70);
final ThemeData gstDarkTheme =
    ThemeData.dark().copyWith(scaffoldBackgroundColor: Colors.black87);

class ThemeController extends GetxController {
  // https://medium.com/swlh/flutter-dynamic-themes-in-3-lines-c3b375f292e3
  static ThemeController get to => Get.find();

  late SharedPreferences prefs;
  late ThemeMode _themeMode;

  ThemeMode get themeMode => _themeMode;

  Future<void> setThemeMode(ThemeMode themeMode) async {
    Get.changeThemeMode(themeMode);
    _themeMode = themeMode;
    update();
    prefs = await SharedPreferences.getInstance();
    await prefs.setString('theme', themeMode.toString().split('.')[1]);
  }

  getThemeModeFromPreferences() async {
    ThemeMode themeMode;
    prefs = await SharedPreferences.getInstance();
    String themeText = prefs.getString('theme') ?? 'light';
    try {
      themeMode =
          ThemeMode.values.firstWhere((e) => describeEnum(e) == themeText);
    } catch (e) {
      themeMode = ThemeMode.light;
    }
    setThemeMode(themeMode);
  }
}
