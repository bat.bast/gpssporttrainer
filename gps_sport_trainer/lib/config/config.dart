import 'dart:math';

class GSTConfig {
  static const int _sensorUpdateInterval = 4000; // Milliseconds
  static const int _durationUpdateInterval = 1000; // Milliseconds
  static int _smoothUpdateDelay =
      2000; // Update values into this interval (milliseconds)
  static int _smoothMaxSteps = 10; // Number of steps used to smooth update
  static const double _smoothMinMetricsUpdate =
      0.1; // Min speed difference use to smooth update

  static const int _mapImageUpdateIntervalMS = 86400000; // One day in ms

  late int _smoothUpdateInterval;
  late int _bootAnimationDuration;
  late Duration _sensorDurationUpdateInterval;

  GSTConfig() {
    _smoothUpdateInterval =
        (min(_smoothUpdateDelay, _sensorUpdateInterval) / _smoothMaxSteps)
            .round();
    _bootAnimationDuration = _smoothUpdateDelay * 2;
    _sensorDurationUpdateInterval =
        const Duration(milliseconds: _sensorUpdateInterval); // Milliseconds
  }

  int get smoothUpdateInterval {
    return _smoothUpdateInterval;
  }

  int get mapImageUpdateIntervalMS {
    return _mapImageUpdateIntervalMS;
  }

  double get bootAnimationDuration {
    return _bootAnimationDuration.toDouble();
  }

  double get minSmoothMetricsUpdate {
    return _smoothMinMetricsUpdate;
  }

  Duration get sensorDurationUpdateInterval {
    return _sensorDurationUpdateInterval;
  }

  int get sensorUpdateInterval {
    return _sensorUpdateInterval;
  }

  void _updateSmoothUpdateInterval() {
    _smoothUpdateInterval =
        (min(_smoothUpdateDelay, _sensorUpdateInterval) / _smoothMaxSteps)
            .round();
  }

  void _updateBootAnimationDuration() {
    _bootAnimationDuration = _smoothUpdateDelay * 2;
  }

  set sensorDurationUpdateInterval(Duration interval) {
    _sensorDurationUpdateInterval = interval;
    _updateSmoothUpdateInterval();
  }

  set smoothUpdateDelay(int delay) {
    _smoothUpdateDelay = delay;
    _updateSmoothUpdateInterval();
    _updateBootAnimationDuration();
  }

  set smoothMaxSteps(int step) {
    _smoothMaxSteps = step;
    _updateSmoothUpdateInterval();
  }

  int get durationUpdateInterval {
    return _durationUpdateInterval;
  }

  int get smoothUpdateDelay {
    return _smoothUpdateDelay;
  }

  int get smoothMaxSteps {
    return _smoothMaxSteps;
  }
}
