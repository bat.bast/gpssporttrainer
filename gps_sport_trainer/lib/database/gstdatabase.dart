import 'dart:async';

import 'package:gps_sport_trainer/course/course.dart';
import 'package:gps_sport_trainer/utils/logger.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class GSTDatabase {
  final Future<Database> _database = getDatabasesPath().then((String path) {
    return openDatabase(
      join(path, 'gps-sport-trainer.db'),
      onCreate: (db, version) {
        return db.execute(
          "CREATE TABLE courses(id TEXT PRIMARY KEY, "
          "startTime TEXT,endTime TEXT, "
          "distance DOUBLE, duration DOUBLE, speedAverage REAL, "
          "icon BLOB, gpx BLOB)",
        );
      },
      version: 1,
    );
  });

  Future<void> insertCourse(GSTCourse course) async {
    final Database db = await _database;
    await db.insert(
      'courses',
      course.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    GSTLogger.logger.d("Insert Course ${course.toString()}");
  }

  Future<List> getCourses() async {
    final Database db = await _database;
    final List<Map> maps = await db.query('courses', columns: [
      'id',
      'startTime',
      'endTime',
      'distance',
      'duration',
      'speedAverage',
      'icon',
    ]);

    GSTLogger.logger.d("Fetched ${maps.length} Courses");
    return maps;
  }

  Future<GSTCourse> loadCourse(String id) async {
    final Database db = await _database;
    final List<Map<String, dynamic>> maps = await db.query(
      'courses',
      columns: ['startTime', 'endTime', 'gpx'],
      where: 'id = ?',
      whereArgs: [id],
    );
    if (maps.isNotEmpty) {
      GSTLogger.logger.d("Loaded Course $id");
      return GSTCourse.buildReferenceCourseForGPX(
        maps.first['startTime'],
        maps.first['endTime'],
        maps.first['gpx'],
      );
    } else {
      return Future<GSTCourse>.value();
    }
  }

  Future<void> deleteCourse(String id) async {
    final Database db = await _database;
    await db.delete(
      'courses',
      where: "id = ?",
      whereArgs: [id],
    );
    GSTLogger.logger.d("Deleted Course $id");
  }
}
