import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:geolocator/geolocator.dart';
import 'package:gps_sport_trainer/config/config.dart';
import 'package:gps_sport_trainer/gst_map_plugin/gst_map_location_options.dart';
import 'package:gps_sport_trainer/gst_map_plugin/gst_map_location_plugin.dart';
import 'package:latlong2/latlong.dart';

class GSTMapWidget extends StatefulWidget {
  final List<LatLng> positions;

  const GSTMapWidget({required this.positions});

  @override
  _GSTMapWidgetState createState() => _GSTMapWidgetState();
}

class _GSTMapWidgetState extends State<GSTMapWidget> {
  final GSTConfig config = GSTConfig();
  final MapController _mapController = MapController();
  final List<Marker> _markers = [];

  late GSTMapLocationOptions _mapLocationOptions;

  // Callback function
  onTapFAB() {
    //userLocationOptions.updateMapLocationOnPositionChange = true;
  }

  @override
  Widget build(BuildContext context) {
    _mapLocationOptions = GSTMapLocationOptions(
        context: context,
        updateMapLocationOnPositionChange: true,
        showMoveToCurrentLocationFloatingActionButton: true,
        zoomToCurrentLocationOnLoad: false,
        mapController: _mapController,
        markers: _markers,
        fabBottom: 20,
        fabRight: 20,
        onTapFAB: onTapFAB,
        onLocationUpdate: (Position pos) =>
            widget.positions.add(LatLng(pos.latitude, pos.longitude)),
        markerWidget: Container(
          height: 10,
          width: 10,
          decoration: const BoxDecoration(
            shape: BoxShape.circle,
            color: Colors.blueAccent,
          ),
        ),
        moveToCurrentLocationFloatingActionButton: Container(
          decoration: BoxDecoration(
              color: Colors.blueAccent,
              borderRadius: BorderRadius.circular(20.0),
              boxShadow: const [
                BoxShadow(
                  color: Colors.grey,
                  blurRadius: 10.0,
                )
              ]),
          child: const Icon(
            Icons.my_location,
            color: Colors.white,
          ),
        ));
    return FlutterMap(
      mapController: _mapController,
      options: MapOptions(
        plugins: [
          GSTMapLocationPlugin(
              location: widget.positions.isNotEmpty
                  ? widget.positions[widget.positions.length - 1]
                  : LatLng(0, 0)),
        ],
      ),
      layers: [
        TileLayerOptions(
          urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
          subdomains: ['a', 'b', 'c'],
          //updateInterval: config.mapImageUpdateIntervalMS,
          tileProvider: const CachedTileProvider(),
        ),
        MarkerLayerOptions(markers: _markers),
        PolylineLayerOptions(
          polylines: [
            Polyline(
                points: widget.positions, strokeWidth: 2.0, color: Colors.red),
          ],
        ),
        _mapLocationOptions
      ],
    );
  }
}

class CachedTileProvider extends TileProvider {
  const CachedTileProvider();

  @override
  ImageProvider getImage(Coords<num> coords, TileLayerOptions options) {
    return CachedNetworkImageProvider(
      getTileUrl(coords, options),
      //Now you can set options that determine how the image gets cached via whichever plugin you use.
    );
  }
}
