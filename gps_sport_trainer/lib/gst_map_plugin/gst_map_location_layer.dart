import 'dart:async';
import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:flutter_map/plugin_api.dart';
import 'package:geolocator/geolocator.dart';
import 'package:gps_sport_trainer/gst_map_plugin/gst_map_location_marker.dart';
import 'package:gps_sport_trainer/gst_map_plugin/gst_map_location_options.dart';
import 'package:gps_sport_trainer/utils/logger.dart';
import 'package:latlong2/latlong.dart';

class GSTMapLayer extends StatefulWidget {
  final LatLng? location;
  final GSTMapLocationOptions options;
  final MapState? map;
  final Stream<Null>? stream;

  GSTMapLayer(
      {Key? key, required this.options, this.map, this.stream, this.location})
      : super(key: key);

  @override
  _GSTMapLayerState createState() => _GSTMapLayerState();
}

class _GSTMapLayerState extends State<GSTMapLayer>
    with TickerProviderStateMixin, WidgetsBindingObserver {
  final double _markerSize = 60.0;
  bool _gpsEnabled = false;
  late StreamSubscription<Position> _positionStream;
  GSTMapLocationMarker? _locationMarker;

  late bool mapLoaded = false;
  late bool _initialStateOfupdateMapLocationOnPositionChange;

  late AnimationController _animationController;

  double _currentDirection = 0;
  LatLng _currentLocation = LatLng(0, 0);

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addObserver(this);
    _initialStateOfupdateMapLocationOnPositionChange =
        widget.options.updateMapLocationOnPositionChange;
    _updatePosition(Position(
      longitude: widget.location!.longitude,
      latitude: widget.location!.latitude,
      timestamp: DateTime.now(),
      accuracy: 0,
      altitude: 0,
      heading: 0,
      speed: 0,
      speedAccuracy: 0,
    ));
    _getGPSPosition();
    setState(() {
      mapLoaded = false;
    });
  }

  @override
  void dispose() {
    _positionStream.cancel();
    //_animationController.dispose();
    WidgetsBinding.instance!.removeObserver(this);
    super.dispose();
  }

  @override
  void setState(fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    if (widget.options.locationUpdateInBackground == false) {
      switch (state) {
        case AppLifecycleState.inactive:
        case AppLifecycleState.paused:
          _positionStream.cancel();
          break;
        case AppLifecycleState.resumed:
          _getGPSPosition();
          break;
        case AppLifecycleState.detached:
          break;
      }
    }
  }

  Future<void> _getGPSPosition() async {
    _gpsEnabled = await Geolocator.isLocationServiceEnabled();
    if (_gpsEnabled) {
      _positionStream =
          Geolocator.getPositionStream().listen((Position position) {
        _addsMarkerLocationToMarkerLocationStream(position);
        _updatePosition(position);
      });
    }
  }

  GSTMapLocationMarker _createGSTMapLocationMarker(
      LatLng location, double direction) {
    return GSTMapLocationMarker(
      height: _markerSize,
      width: _markerSize,
      point: LatLng(location.latitude, location.longitude),
      builder: (context) {
        return Stack(
          alignment: AlignmentDirectional.center,
          children: <Widget>[
            if (widget.options.showHeading)
              ClipOval(
                child: Transform.rotate(
                  angle: direction / 180 * math.pi,
                  child: CustomPaint(
                    size: Size(_markerSize, _markerSize),
                    painter: GSTMapDirectionMarker(),
                  ),
                ),
              ),
            Container(
              height: 20.0,
              width: 20.0,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.blue[300]!.withOpacity(0.7),
              ),
            ),
            widget.options.markerWidget
          ],
        );
      },
    );
  }

  void _updateLocationMarker(LatLng location, double direction) {
    widget.options.markers.remove(_locationMarker);
    _locationMarker = _createGSTMapLocationMarker(location, direction);
    widget.options.markers.add(_locationMarker!);
  }

  void _updatePosition(Position position) {
    setState(() {
      bool newDirection = false;
      bool newPosition = false;
      if (position.heading != _currentDirection) {
        GSTLogger.logger.d(
            'Update Direction: ${_currentDirection.toString()} ⟶ ${position.heading.toString()}');
        _currentDirection = position.heading;
        newDirection = true;
      }
      if (position.latitude != _currentLocation.latitude &&
          position.longitude != _currentLocation.longitude) {
        _currentLocation = LatLng(position.latitude, position.longitude);
        newPosition = true;
      }
      if (newDirection || newPosition) {
        _updateLocationMarker(_currentLocation, _currentDirection);
      }

      if (newPosition) {
        if (widget.options.updateMapLocationOnPositionChange) {
          _moveMapToCurrentLocation();
        } else if (widget.options.updateMapLocationOnPositionChange) {
          if (!widget.options.updateMapLocationOnPositionChange) {
            widget.map!.fitBounds(widget.map!.bounds, const FitBoundsOptions());
          }
          GSTLogger.logger.w(
              "Warning: updateMapLocationOnPositionChange set to true, but no mapController provided: can't move map");
        } else {
          _forceMapUpdate();
        }

        if (widget.options.zoomToCurrentLocationOnLoad && (!mapLoaded)) {
          setState(() {
            mapLoaded = true;
          });
          animatedMapMove(_currentLocation, widget.options.defaultZoom,
              widget.options.mapController, this);
        }
      }
    });
  }

  void _moveMapToCurrentLocation({double? zoom}) {
    animatedMapMove(
      _currentLocation,
      zoom ?? widget.map!.zoom,
      widget.options.mapController,
      this,
    );
  }

  _addsMarkerLocationToMarkerLocationStream(Position position) {
    // Only update if Location changed (eliminate situation without movement)
    if (position.latitude != _currentLocation.latitude &&
        position.longitude != _currentLocation.longitude) {
      widget.options.onLocationUpdate(position);
    }
  }

  @override
  Widget build(BuildContext context) {
    return widget.options.showMoveToCurrentLocationFloatingActionButton
        ? Positioned(
            bottom: widget.options.fabBottom,
            right: widget.options.fabRight,
            height: widget.options.fabHeight,
            width: widget.options.fabWidth,
            child: InkWell(
                hoverColor: Colors.blueAccent[200],
                onTap: () {
                  if (_initialStateOfupdateMapLocationOnPositionChange) {
                    setState(() {
                      widget.options.updateMapLocationOnPositionChange = false;
                    });
                  }
                  //_getGPSPosition();
                  _moveMapToCurrentLocation(zoom: widget.options.defaultZoom);
                  widget.options.onTapFAB();
                },
                child:
                    widget.options.moveToCurrentLocationFloatingActionButton),
          )
        : Container();
  }

  void animatedMapMove(
      LatLng destLocation, double destZoom, _mapController, vsync) {
    // In our case, we want to split the transition between our current map center and the destination.
    final _latTween = Tween<double>(
        begin: _mapController.center.latitude, end: destLocation.latitude);
    final _lngTween = Tween<double>(
        begin: _mapController.center.longitude, end: destLocation.longitude);
    final _zoomTween = Tween<double>(begin: _mapController.zoom, end: destZoom);

    _animationController = AnimationController(
        duration: const Duration(milliseconds: 500), vsync: vsync);
    // The animation determines what path the animation will take. You can try different Curves values, although I found
    // fastOutSlowIn to be my favorite.
    Animation<double> animation = CurvedAnimation(
        parent: _animationController, curve: Curves.fastOutSlowIn);

    _animationController.addListener(() {
      _mapController.move(
          LatLng(_latTween.evaluate(animation), _lngTween.evaluate(animation)),
          _zoomTween.evaluate(animation));
    });

    animation.addStatusListener((status) {
      if (status == AnimationStatus.completed ||
          status == AnimationStatus.dismissed) {
        if (_initialStateOfupdateMapLocationOnPositionChange) {
          setState(() {
            widget.options.updateMapLocationOnPositionChange = true;
          });
        }
        _animationController.dispose();
      }
    });
    _animationController.forward();
  }

  void _forceMapUpdate() {
    var zoom = widget.options.mapController.zoom;
    widget.options.mapController.move(widget.options.mapController.center,
        widget.options.mapController.zoom + 0.000001);
    widget.options.mapController
        .move(widget.options.mapController.center, zoom);
  }
}

class GSTMapDirectionMarker extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final Rect rect = Rect.fromCircle(
      center: const Offset(30.0, 30.0),
      radius: 40.0,
    );

    final Gradient gradient = RadialGradient(
      colors: <Color>[
        Colors.blue.shade500.withOpacity(0.6),
        Colors.blue.shade500.withOpacity(0.3),
        Colors.blue.shade500.withOpacity(0.1),
      ],
      stops: const <double>[
        0.0,
        0.5,
        1.0,
      ],
    );

    final Paint paint = Paint()..shader = gradient.createShader(rect);
    canvas.drawArc(rect, pi * 6 / 5, pi * 3 / 5, true, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
