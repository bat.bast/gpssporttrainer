import 'package:flutter_map/flutter_map.dart';

class GSTMapLocationMarker extends Marker {
  GSTMapLocationMarker({
    point,
    builder,
    width,
    height,
    AnchorPos? anchorPos,
  }) : super(
          point: point,
          builder: builder,
          width: width,
          height: height,
          anchorPos: anchorPos,
        );
}
