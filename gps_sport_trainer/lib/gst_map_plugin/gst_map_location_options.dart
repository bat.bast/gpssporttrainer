import 'package:flutter/material.dart';
import 'package:flutter_map/plugin_api.dart';
import 'package:geolocator/geolocator.dart';

class GSTMapLocationOptions extends LayerOptions {
  BuildContext context;
  List<Marker?> markers;
  MapController mapController;

  Widget markerWidget;
  bool updateMapLocationOnPositionChange;
  bool showMoveToCurrentLocationFloatingActionButton;
  bool zoomToCurrentLocationOnLoad;
  bool enableMultiFingerGestureRace;
  Widget? moveToCurrentLocationFloatingActionButton;

  void Function(Position position) onLocationUpdate;
  void Function() onTapFAB;

  double fabBottom;
  double fabRight;
  double fabHeight;
  double fabWidth;

  bool showHeading;

  double defaultZoom;

  // If false the location update stream is paused if the app is in the
  // background (app lifecycle state inactive and paused). Once the app is
  // resumed the stream is resumed as well.
  bool locationUpdateInBackground;

  Duration sensorUpdateInterval;

  GSTMapLocationOptions(
      {required this.context,
      required this.markers,
      required this.mapController,
      required this.markerWidget,
      required this.onLocationUpdate,
      required this.onTapFAB,
      this.moveToCurrentLocationFloatingActionButton,
      this.enableMultiFingerGestureRace = true,
      this.updateMapLocationOnPositionChange = true,
      this.showMoveToCurrentLocationFloatingActionButton = true,
      this.fabBottom = 20,
      this.fabHeight = 40,
      this.fabRight = 20,
      this.fabWidth = 40,
      this.defaultZoom = 15,
      this.zoomToCurrentLocationOnLoad = true,
      this.showHeading = true,
      this.locationUpdateInBackground = false,
      this.sensorUpdateInterval = const Duration(milliseconds: 4000)});
}
