import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_map/plugin_api.dart';
import 'package:gps_sport_trainer/gst_map_plugin/gst_map_location_layer.dart';
import 'package:gps_sport_trainer/gst_map_plugin/gst_map_location_options.dart';
import 'package:latlong2/latlong.dart';

class GSTMapLocationPlugin implements MapPlugin {
  LatLng location;

  GSTMapLocationPlugin({required this.location});

  @override
  Widget createLayer(
      LayerOptions options, MapState mapState, Stream<Null> stream) {
    if (options is GSTMapLocationOptions) {
      return GSTMapLayer(
          options: options, map: mapState, stream: stream, location: location);
    }
    throw Exception(
        'Unknown options type for GSTUserLocationPlugin map plugin: $options');
  }

  @override
  bool supportsLayer(LayerOptions options) {
    return options is GSTMapLocationOptions;
  }
}
