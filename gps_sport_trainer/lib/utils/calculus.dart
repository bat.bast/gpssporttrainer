import 'package:geolocator/geolocator.dart';

double speedKMPerHourFromPosition(Position position) {
  return position.speed >= 0
      ? position.speed * 3.6
      : 0.0; // GPS speed is in m/s --> km/h
}

Duration differenceDateTime(DateTime? start, DateTime? end) {
  if (start != null && end != null) {
    return start.difference(end);
  } else {
    return const Duration(microseconds: 0);
  }
}

double distanceMeterFromPositions(Position? start, Position? end) {
  if (start != null && end != null) {
    return Geolocator.distanceBetween(
        start.latitude, start.longitude, end.latitude, end.longitude);
  } else {
    return 0.0;
  }
}

double averageSpeedKMPerHour(Duration? duration, double distanceMeter) {
  // Use absolute value, sometimes - 0.0 is the results (math round near zero)
  return (distanceMeter * 3600000 / duration!.inMicroseconds).abs();
}
