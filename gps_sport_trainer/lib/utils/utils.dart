import 'package:intl/intl.dart';

String getDateStr(DateTime dt) {
  var ts = dt.toString();
  return ts.substring(0, ts.indexOf('.'));
}

String buildCourseTitle(String startTimeStr, String endTimeStr) {
  try {
    DateTime startTime = DateTime.parse(startTimeStr);
    DateTime endTime = DateTime.parse(endTimeStr);
    final DateFormat formatterYMDHMS = DateFormat('yyyy-MM-dd HH:mm:ss');
    final DateFormat formatterHMS = DateFormat('HH:mm:ss');
    String resultTitle = "${formatterYMDHMS.format(startTime)} ➼ ";
    if (startTime.year == endTime.year &&
        startTime.month == endTime.month &&
        startTime.day == endTime.day) {
      resultTitle += formatterHMS.format(endTime);
    } else {
      resultTitle += formatterYMDHMS.format(endTime);
    }
    return resultTitle;
  } catch (formatException) {
    return "$startTimeStr ➼ $endTimeStr";
  }
}

String getDateTimeStr(DateTime dt) {
  var ts = dt.toString();
  return ts;
}

String getDurationStr(Duration dt) {
  var ts = dt.toString();
  return ts.substring(0, ts.indexOf('.'));
}

String getDistanceMStr(double distanceMeters) {
  return distanceMeters.toStringAsFixed(0);
}

String getNowStr() {
  var n = DateTime.now();
  var resH = n.hour < 10 ? '0' + n.hour.toString() : n.hour.toString();
  var resM = n.minute < 10 ? '0' + n.minute.toString() : n.minute.toString();
  return resH + ':' + resM;
}

String getDistanceMetersStr(double distance) {
  return distance.toStringAsFixed(0);
}

String getDistanceKiloMetersStr(double distance) {
  return (distance / 1000).toStringAsFixed(3);
}

String getSpeedStr(double speed) {
  return speed.toStringAsFixed(1);
}
